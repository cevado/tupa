defmodule Tupa do
  def validate(data, structure) do
    if valid_structure?(structure) do
      data
      |> build_data(structure)
      |> error_hyegene()
      |> define_type()
    else
      %{data: nil, type: :invalid, errors: [structure: :invalid]}
    end
  end

  defp valid_structure?(structure) when is_map(structure) do
    Enum.all?(structure, &valid_types?/1)
  end

  defp valid_structure?(structure) when is_atom(structure), do: valid_types?({nil, structure})
  defp valid_structure?(_), do: false

  defp valid_types?({_, :number}), do: true
  defp valid_types?({_, :literal}), do: true
  defp valid_types?({_, :string}), do: true
  defp valid_types?({_, :list}), do: true
  defp valid_types?({_, :map}), do: true
  defp valid_types?({_, :boolean}), do: true
  defp valid_types?({_, {:list, structure}}), do: valid_structure?(structure)
  defp valid_types?({_, {:map, structure}}), do: valid_structure?(structure)
  defp valid_types?({_, structure}) when is_map(structure), do: valid_structure?(structure)
  defp valid_types?(_), do: false

  defp build_data(data, structure) when is_atom(structure) do
    if valid_value?(data, structure) do
      %{data: data, errors: []}
    else
      %{data: data, errors: [data: :invalid]}
    end
  end

  defp build_data(data, structure) when is_map(data) and is_map(structure) do
    structure
    |> Enum.map(&build_field(&1, data))
    |> Enum.reduce(%{}, &merge_data/2)
  end

  defp build_data(_data, _strucutre), do: %{data: nil, errors: [data: :invalid]}

  defp build_field({key, type}, data) do
    value = data[key] || data[to_string(key)]

    case valid_value?(value, type) do
      true ->
        %{data: %{key => value}, errors: []}

      false ->
        %{data: %{key => nil}, errors: [{key, :invalid}]}

      :map ->
        {:map, structure} = map_type(type)
        %{data: value, errors: errors} = build_data(value, structure)
        %{data: %{key => value}, errors: errors}

      :list ->
        {:list, structure} = type

        data =
          value
          |> List.wrap()
          |> Enum.map(&build_data(&1, structure))

        errors = Enum.map(data, & &1[:errors])
        value = Enum.map(data, & &1[:data])

        %{data: %{key => value}, errors: [{key, errors}]}
    end
  end

  defp map_type(map) when is_map(map), do: {:map, if(Map.keys(%{}) == [], do: :map, else: map)}
  defp map_type(type), do: type

  defp merge_data(%{data: data, errors: errors}, acc) do
    acc
    |> Map.update(:data, data, &Map.merge(&1, data))
    |> Map.update(:errors, errors, &Keyword.merge(&1, errors))
  end

  defp valid_value?(value, :number), do: is_number(value)
  defp valid_value?(value, :literal), do: is_boolean(value) or is_nil(value)
  defp valid_value?(value, :string), do: is_bitstring(value)
  defp valid_value?(value, :list), do: is_list(value)
  defp valid_value?(value, :map), do: is_map(value)
  defp valid_value?(value, %{}), do: is_map(value) && :map
  defp valid_value?(value, {:map, _}), do: is_map(value) && :map
  defp valid_value?(value, {:list, _}), do: is_list(value) && :list

  defp error_hyegene(%{errors: []} = data), do: data

  defp error_hyegene(%{errors: errors} = data) do
    errors =
      errors
      |> cleanup_errors()
      |> Enum.reject(&nil_or_empty_list/1)
      |> List.flatten()

    %{data | errors: errors}
  end

  defp error_hyegene(data), do: data

  defp cleanup_errors([]), do: nil

  defp cleanup_errors(errors) do
    errors
    |> Enum.map(&cleanup_error/1)
    |> Enum.reject(&nil_or_empty_list/1)
    |> List.flatten()
  end

  defp nil_or_empty_list(nil), do: true
  defp nil_or_empty_list([]), do: true
  defp nil_or_empty_list({_, []}), do: true
  defp nil_or_empty_list(_), do: false

  defp cleanup_error({_, []}), do: nil
  defp cleanup_error({_, nil}), do: nil
  defp cleanup_error({field, errors}) when is_list(errors), do: {field, cleanup_errors(errors)}
  defp cleanup_error(error), do: error

  defp define_type(%{errors: nil} = data), do: Map.put(data, :type, :valid)
  defp define_type(%{errors: []} = data), do: Map.put(data, :type, :valid)
  defp define_type(data), do: Map.put(data, :type, :invalid)
end
