defmodule NamedSpaced do
  defmacro n(a) do
    [m, f] = String.split(a, "/")
    f = :"#{f}"

    quote do
      {unquote(m), unquote(f)}
    end
  end

  defmacro call(a, b) do
    {m, f} = Macro.expand(a, __CALLER__)
    {{:., [], [{:__aliases__, [alias: false], [:"#{m}"]}, f]}, [], b}
  end
end
